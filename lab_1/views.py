from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Alfian Fuadi Rafli' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 11, 17) #TODO Implement this, format (Year, Month, Date)
npm = 1706028650 # TODO Implement this
tempat_kuliah = 'Fasilkom UI'
hobi = 'Tenis Meja'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'tk': tempat_kuliah, 'hobi': hobi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
