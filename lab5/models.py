from django.db import models

# Create your models here.
class Schedule(models.Model):
    activity = models.CharField(max_length=50)
    date = models.DateTimeField()
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50)

    def __str__(self):
        return self.activity