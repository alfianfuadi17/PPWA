from django.shortcuts import render
from django.shortcuts import redirect
from .form import ScheduleForm
from .models import Schedule

# Create your views here.
my_name = 'Alfian Fuadi Rafli'

def index(request):
	response = {'name': my_name}
	return render(request, "index.html", response)

def ScheduleFormViews(request):
    response = {'form': ScheduleForm}
    return render(request, "scheduleForm.html", response)

def CreateSchedule(request):
    schedule = ScheduleForm(request.POST)
    if (request.method == 'POST' and schedule.is_valid()):
        aktivitas = request.POST['activity']
        tanggal = request.POST['date']
        lokasi = request.POST['location']
        kategori = request.POST['category']
        Schedule.objects.create(
            activity = aktivitas, 
            location = lokasi, 
            category = kategori,
            date = tanggal
        )
        return redirect('lab5:schedule')
    else:
        return redirect('lab5:schedule-form')

def MySchedule(request):
    response = {}
    data = Schedule.objects.all()
    response['data'] = data
    return render(request, "schedule.html", response)

def DeleteAll(request):
    Schedule.objects.all().delete()
    return redirect('lab5:schedule')
