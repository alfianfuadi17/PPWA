from django import forms

class ScheduleForm(forms.Form):
    activity = forms.CharField(label='Activity:', max_length=50, required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    location = forms.CharField(label='Location:', max_length=50, required=True,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    category = forms.CharField(label='Category:', max_length=50, required=True,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}),
                               input_formats=['%Y-%m-%dT%H:%M'])
