from django.urls import path

from . import views

app_name = 'lab5'
urlpatterns = [
	path('schedule-form/', views.ScheduleFormViews, name='schedule-form'),
	path('create-schedule/', views.CreateSchedule, name='add_activity'),
	path('delete-all/', views.DeleteAll, name ='delete_all'),
	path('my-schedule/', views.MySchedule, name='schedule'),
	path('', views.index, name='index'),
]